package com.example.springbootemail.controller;

//import com.SpringBootEmail.Entity.EmailDetails;
//import com.SpringBootEmail.service.EmailService;
import com.example.springbootemail.model.EmailDetails;
import com.example.springbootemail.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {

    @Autowired private EmailService emailService;


    // Sending email with attachment
    @PostMapping("/api/v1/mail")
    public String sendMailWithAttachment(
            @RequestBody EmailDetails details)
    {
        String status
                = emailService.sendMailWithAttachment(details);

        return status;
    }
}
