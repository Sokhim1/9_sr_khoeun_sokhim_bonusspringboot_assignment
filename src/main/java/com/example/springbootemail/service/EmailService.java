package com.example.springbootemail.service;
//import com.SpringBootEmail.Entity.EmailDetails;
import com.example.springbootemail.model.EmailDetails;

public interface EmailService {

//    String sendSimpleMail(EmailDetails details);

    String sendMailWithAttachment(EmailDetails details);
}
