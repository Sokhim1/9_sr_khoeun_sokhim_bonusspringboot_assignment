package com.example.springbootemail.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;

@Data
@NoArgsConstructor
@AutoConfigureAfter
public class EmailDetails {
    private String toEmail;
    private String senderName;
    private String subject;
    private String body;
}
